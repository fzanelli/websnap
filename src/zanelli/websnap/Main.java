/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zanelli.websnap;

import com.teamdev.jxbrowser.chromium.BrowserType;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;
import com.teamdev.jxbrowser.chromium.javafx.internal.LightWeightWidget;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.imageio.ImageIO;

public class Main extends Application {

    static private Scene scene;
    static private Browser b;
    static private Browser b2;

    private void showControlStage(final Stage owner) {
        Button snapshotButton = new Button("Take Snapshot");
        snapshotButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                WritableImage snapshot = b.snapshot(null, null);
                File file = new File("c:/users/public/test.jpg");
                BufferedImage rr = SwingFXUtils.fromFXImage(snapshot, null);
                BufferedImage ra = rr;
                ra = new BufferedImage(rr.getWidth(), rr.getHeight(), BufferedImage.TYPE_INT_RGB);

                Graphics2D graphics = ra.createGraphics();

                graphics.drawImage(
                        rr,
                        0,
                        0,
                        null);

                try {
                    ImageIO.write(
                            ra,
                            "jpeg",
                            file);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        Button hideshow = new Button("Hide/Show");
        hideshow.setOnAction((event) -> {
//            scene.sho
        });

        VBox layout = new VBox(20);
        layout.setAlignment(Pos.CENTER);
        layout.setStyle("-fx-background-color: cornsilk; -fx-padding: 10;");
        layout.getChildren().setAll(snapshotButton);

        Stage controlStage = new Stage(StageStyle.UTILITY);
//        Stage controlStage = new Stage();
        controlStage.initOwner(owner);
        controlStage.setX(0);
        controlStage.setY(0);
        controlStage.setTitle("Control");
        controlStage.setScene(new Scene(layout));
        controlStage.setAlwaysOnTop(true);
        controlStage.show();
    }

    @Override
    public void start(Stage stage) throws IOException {
        com.teamdev.jxbrowser.chromium.Browser jxb = new com.teamdev.jxbrowser.chromium.Browser(BrowserType.LIGHTWEIGHT);
        BrowserView jxbv = new BrowserView(jxb);
        // create the scene
        stage.setTitle("Web View");
        b = new Browser();
        b2 = new Browser();
        TextField tf = new TextField();
        tf.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    jxb.loadURL(tf.getText());
                }
            }
        });

        b.webEngine.locationProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                tf.setText(newValue);
            }
        });

        final HBox hb = new HBox(b, b2);
//        final HBox hb = new HBox(b);

//        VBox vb = new VBox(tf, jxbv);
        BorderPane bp = new BorderPane(jxbv);
        bp.setTop(tf);

        scene = new Scene(bp, 800, 600, Color.web("#666970"));
        stage.setScene(scene);

        stage.initStyle(StageStyle.UTILITY);
        stage.show();

        jxb.loadURL("http://www.google.com");
        showControlStage(null);

        final AtomicBoolean updated = new AtomicBoolean(false);

        class CompleteService extends ScheduledService<Object> {

            @Override
            protected Task<Object> createTask() {
                return new Task<Object>() {
                    @Override
                    protected Object call() throws Exception {
                        File file = new File("c:/users/public/test.jpg");

                        FutureTask<Image> snapTaker = new FutureTask<>(new Callable<Image>() {
                            @Override
                            public Image call() throws Exception {
                                LightWeightWidget lightWeightWidget = (LightWeightWidget) jxbv.getChildren().get(0);
                                Image image = lightWeightWidget.getImage();
                                return image;
                            }
                        });

                        Platform.runLater(snapTaker);

                        try {
                            BufferedImage rr = SwingFXUtils.fromFXImage(snapTaker.get(), null);
                            BufferedImage ra = new BufferedImage(rr.getWidth(), rr.getHeight(), BufferedImage.TYPE_INT_RGB);
                            Graphics2D graphics = ra.createGraphics();

                            graphics.drawImage(
                                    rr,
                                    0,
                                    0,
                                    null);

                            ImageIO.write(ra, "jpeg", file);                                                        
                        } catch (Exception ex) {
                            ex.printStackTrace();                            
                        } 
                       
                        if ((System.currentTimeMillis() / 60000) % 5 == 0) {
                            if (updated.compareAndSet(false, true)) {
                                jxb.reload();
                            }
                        } else {
                            updated.set(false);
                        }

                        return file;
                    }

                };
            }
        }

        final AtomicInteger count = new AtomicInteger(0);
        class SimpleService extends ScheduledService {

            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                System.out.printf("%03d - %s - Reload\n", count.incrementAndGet(), new Date());
                                jxb.reload();
                            }
                        });
                        return null;
                    }
                };
            }
        };
        ScheduledService myService;
//        myService = new CompleteService();
        myService = new SimpleService();

        myService.setPeriod(Duration.seconds(5));
        myService.start();

    }

    public static void main(String[] args) {
        launch(args);

    }

}

class Browser extends Region {

    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();

    public Browser() {
        //apply the styles
        getStyleClass().add("browser");
        // load the web page
        webEngine.load("https://www.sunnyportal.com/FixedPages/HoManEnergyRedesign.aspx");
        //add the web view to the scene
        getChildren().add(browser);

    }

    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser, 0, 0, w, h, 0, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefWidth(double height) {
        return 750;
    }

    @Override
    protected double computePrefHeight(double width) {
        return 1024;
    }
}
